/* errors */

App.errors = (function() {

    var _id = 'error-alert',
        _alert,
        _titleEl,
        _contentEl,
        _callback,
        _fs = Fs,
        _history = _fs.History;

    var _getAlert = function() {
        if (!_alert) {
            _alert = new _fs.views.Alert({
                config: {
                    id: _id,
                    overlayHide: false,
                    ui: 'd',
                    header: {
                        title: 'Error',
                        ui: 'd'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                items: [
                    '<p id="' + _id + '-content"></p>', {
                    xtype: 'button',
                    config: {
                        text: 'OK',
                        ui: 'c',
                        inline: false,
                        style: 'margin: 0 auto;'
                    },
                    handler: function() {
                        if (_callback) {
                            _callback();
                        }
                        _alert.hide();
                    }
                }]
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
            _titleEl = _fs.Selector.get('.ui-title', document.getElementById(_id));
            _contentEl = document.getElementById(_id + '-content');
        }
        return _alert;
    };

    var _showAlert = function(title, msg, callback) {
        if (!_alert) {
            _getAlert();
        }
        _callback = callback;
        _fs.Selector.updateHtml(_titleEl, title);
        _fs.Selector.updateHtml(_contentEl, _formatMessage(msg));
        _getAlert().show();
    };

    var _formatMessage = function(msg) {
        var type = typeof msg;

        if (type === 'string') {
            return msg;
        } else if (type === 'object' &&
            typeof msg.join === 'function') {
            return msg.join('\n');
        } else {
            var i, result = '';

            for (i in msg) {
                result += '<b>' + i + '</b>: ' + msg[i].join(' and ') + '<br>';
            }
            return result;
        }
    };

    var _defaultBehavior = function(requestSuccess, responseData) {
        if (requestSuccess) {
            var message = responseData.message;

            if (responseData.logout) {
                _showAlert(t('Error'),
                    t('You has been logged out, please sign in again.'), function() {
                    App.Settings.set('currentUser', null);
                    _history.navigate('/login?back=' + encodeURIComponent(_history.here()));
                });
            } else {
                _showAlert(t('Response error'), t(message ? message : 'Unknown error.'));
            }
        } else {
            _showAlert(t('Server error'),
                t('Unable to reach server.'));
        }
    };

    var _onStoreError = function(requestSuccess, store, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };

    var _onJsonpError = function(requestSuccess, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };

    return {
        onStoreError: _onStoreError,
        onJsonpError: _onJsonpError,
        showAlert: _showAlert
    };
})();