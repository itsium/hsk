/* Main controller */

App.controllers.Main = (function() {
    // private
    var _layoutPage,
        _mainMenu,
        _activeTab,
        _lastPage,
        _pages = {},
        _fs = Fs,
        _history = _fs.History,
        _parent = _fs.Router,
        _app = App,
        _appViews = _app.views,
        _settings = _app.Settings,
        _selector = _fs.Selector,
        _scrollPos = {},
        _activeBtnClass = 'ui-btn-active';


    var _routeGenerate = function(name, parent) {
        return function() {
            var pageId = 'page-' + name,
                p = _pages[pageId],
                tabname = 'menu-' + (parent ? parent : name) + '-btn';

            // @TODO bugfix !p.html.length because sometimes compile is not done properly.
            if (!p) {
                _pages[pageId] = new _appViews[_fs.utils.capitalize(name)]({
                    config: {
                        id: pageId,
                        hidden: true
                    }
                });
                p = _pages[pageId];
            }
            if (!p.html.length) {
                p.compile();
                p.render('#page-footer', 'beforebegin');
            }
            this.changeActiveTab(tabname);
            this.switchPage(pageId, arguments);
        };
    };

    // public
    return _parent.subclass({

        routes: {
            '': 'play',
            '/play': 'play'
        },

        constructor: function() {
            if (!_layoutPage) {
                _layoutPage = new _appViews.Layout({
                    config: {
                        id: 'mainpage'
                    }
                });
                _layoutPage.compile();
                _layoutPage.render('body');
            }
            _mainMenu = document.getElementById('main-menu');
            _parent.prototype.constructor.apply(this, arguments);
            _fs.History.start();
        },

        changeActiveTab: function(newTab) {
            var ntab = document.getElementById(newTab);

            if (!ntab) {
                return;
            }
            var activeTabs = _selector.gets('.' + _activeBtnClass,
                document.getElementById('main-menu')),
                length = activeTabs.length;

            while (length--) {
                _selector.removeClass(activeTabs[length], _activeBtnClass);
            }
            _selector.addClass(ntab, _activeBtnClass);
            _activeTab = newTab;
        },

        play: _routeGenerate('play'),

        switchPage: function(newPage, routeArgs) {
            var w = window,
                fromPage = _pages[_lastPage],
                toPage = _pages[newPage];

            if (_lastPage) {
                _scrollPos[_lastPage] = w.scrollY;
                _selector.addClass(document.getElementById(_lastPage),
                    'hidden');
                if (fromPage) {
                    fromPage.fire('hide', 0, _scrollPos[_lastPage]);
                }
            }
            _selector.removeClass(document.getElementById(newPage),
                'hidden');
            w.scrollTo(0, _scrollPos[newPage] || 0);
            _lastPage = newPage;
            if (toPage) {
                toPage.fire('show', routeArgs);
            }
        }
    });
}());