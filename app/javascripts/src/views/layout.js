/* Layout page */

App.views.Layout = (function() {
    var _tabs,
        _header,
        _footer,
        _fs = Fs,
        _iconPos = 'top',
        _showText = false,
        _parent = _fs.views.Page;

    _tabs = {
        xtype: 'tabs',
        config: {
            id: 'main-menu',
            //cssClass: 'largetabs',
            mini: false
        },
        items: [{
            xtype: 'tabbutton',
            config: {
                id: 'menu-play-btn',
                icon: '&#128240;',
                route: '/play',
                text: (_showText ? t('News') : undefined),
                iconPos: _iconPos
            }
        }]
    };

    _footer = {
        xtype: 'footer',
        config: {
            ui: 'f',
            id: 'page-footer',
            position: 'fixed',
            style: 'display: none'
        },
        items: _tabs
    };

    return _parent.subclass({
        constructor: function(opts) {
            opts = _fs.utils.applyIfAuto(opts || {}, {
                items: _footer
            });
            _parent.prototype.constructor.call(this, opts);
        }
    });
}());