App.views.Play = (function() {

    var _wordchoiceEl,
        _currentAnswer,
        _audioEl,
        _helpEl,
        _hasTranslate = false,
        _hasPinyin = false,
        _wordsEl = [],
        _fs = Fs,
        _win = window,
        _selector = _fs.Selector,
        _parent = _fs.views.Container,
        _words = [{"id":1,"chinese":"爱","pinyin":"Ài","english":"Love","length":1},{"id":2,"chinese":"八","pinyin":"Bā","english":"Eight","length":1},{"id":3,"chinese":"爸爸","pinyin":"Bàba","english":"Father","length":2},{"id":4,"chinese":"杯子","pinyin":"Bēizi","english":"Cup","length":2},{"id":5,"chinese":"北京","pinyin":"Běijīng","english":"Beijing","length":2},{"id":6,"chinese":"本","pinyin":"Běn","english":"This","length":1},{"id":7,"chinese":"不","pinyin":"Bù","english":"Not","length":1},{"id":8,"chinese":"不客气","pinyin":"Bù kèqì","english":"You're welcome","length":3},{"id":9,"chinese":"菜","pinyin":"Cài","english":"Dish","length":1},{"id":10,"chinese":"茶","pinyin":"Chá","english":"Tea","length":1},{"id":11,"chinese":"吃","pinyin":"Chī","english":"Eat","length":1},{"id":12,"chinese":"出租车","pinyin":"Chūzū chē","english":"Taxi","length":3},{"id":13,"chinese":"打电话","pinyin":"Dǎ diànhuà","english":"Phone","length":3},{"id":14,"chinese":"大","pinyin":"Dà","english":"Large","length":1},{"id":15,"chinese":"的","pinyin":"De","english":"Of","length":1},{"id":16,"chinese":"点","pinyin":"Diǎn","english":"Point","length":1},{"id":17,"chinese":"电脑","pinyin":"Diànnǎo","english":"Computer","length":2},{"id":18,"chinese":"电视","pinyin":"Diànshì","english":"TV","length":2},{"id":19,"chinese":"电影","pinyin":"Diànyǐng","english":"Movie","length":2},{"id":20,"chinese":"东西","pinyin":"Dōngxi","english":"Stuff","length":2},{"id":21,"chinese":"都","pinyin":"Dōu","english":"Both","length":1},{"id":22,"chinese":"读","pinyin":"Dú","english":"Read","length":1},{"id":23,"chinese":"对不起","pinyin":"Duìbùqǐ","english":"I am sorry","length":3},{"id":24,"chinese":"多","pinyin":"Duō","english":"Multi-","length":1},{"id":25,"chinese":"多少","pinyin":"Duōshǎo","english":"How much","length":2},{"id":26,"chinese":"儿子","pinyin":"Érzi","english":"Son","length":2},{"id":27,"chinese":"二","pinyin":"Èr","english":"Two","length":1},{"id":28,"chinese":"饭馆","pinyin":"Fànguǎn","english":"Restaurant","length":2},{"id":29,"chinese":"飞机","pinyin":"Fēijī","english":"Aircraft","length":2},{"id":30,"chinese":"分钟","pinyin":"Fēnzhōng","english":"Minute","length":2},{"id":31,"chinese":"高兴","pinyin":"Gāoxìng","english":"Happy","length":2},{"id":32,"chinese":"个","pinyin":"Gè","english":"A","length":1},{"id":33,"chinese":"工作","pinyin":"Gōngzuò","english":"Work","length":2},{"id":34,"chinese":"狗","pinyin":"Gǒu","english":"Dog","length":1},{"id":35,"chinese":"汉语","pinyin":"Hànyǔ","english":"Chinese","length":2},{"id":36,"chinese":"好","pinyin":"Hǎo","english":"Good","length":1},{"id":37,"chinese":"喝","pinyin":"Hē","english":"Drink","length":1},{"id":38,"chinese":"和","pinyin":"Hé","english":"And","length":1},{"id":39,"chinese":"很","pinyin":"Hěn","english":"Very","length":1},{"id":40,"chinese":"后面","pinyin":"Hòumiàn","english":"Behind","length":2},{"id":41,"chinese":"回","pinyin":"Huí","english":"Return","length":1},{"id":42,"chinese":"会","pinyin":"Huì","english":"Can","length":1},{"id":43,"chinese":"火车站","pinyin":"Huǒchē zhàn","english":"Railway station","length":3},{"id":44,"chinese":"几","pinyin":"Jǐ","english":"A few","length":1},{"id":45,"chinese":"家","pinyin":"Jiā","english":"Home","length":1},{"id":46,"chinese":"叫","pinyin":"Jiào","english":"Call","length":1},{"id":47,"chinese":"她","pinyin":"Tā","english":"She","length":1},{"id":48,"chinese":"今天","pinyin":"Jīntiān","english":"Today","length":2},{"id":49,"chinese":"九","pinyin":"Jiǔ","english":"Nine","length":1},{"id":50,"chinese":"开","pinyin":"Kāi","english":"Open","length":1},{"id":51,"chinese":"看","pinyin":"Kàn","english":"Look","length":1},{"id":52,"chinese":"看见","pinyin":"Kànjiàn","english":"See","length":2},{"id":53,"chinese":"块","pinyin":"Kuài","english":"Block","length":1},{"id":54,"chinese":"来","pinyin":"Lái","english":"Come","length":1},{"id":55,"chinese":"老师","pinyin":"Lǎoshī","english":"Teacher","length":2},{"id":56,"chinese":"冷","pinyin":"Lěng","english":"Cold","length":1},{"id":57,"chinese":"里","pinyin":"Lǐ","english":"In","length":1},{"id":58,"chinese":"了","pinyin":"Le","english":"The","length":1},{"id":59,"chinese":"零","pinyin":"Líng","english":"Zero","length":1},{"id":60,"chinese":"六","pinyin":"Liù","english":"Six","length":1},{"id":61,"chinese":"妈妈","pinyin":"Māmā","english":"Mom","length":2},{"id":62,"chinese":"吗","pinyin":"Ma","english":"Do","length":1},{"id":63,"chinese":"买","pinyin":"Mǎi","english":"Buy","length":1},{"id":64,"chinese":"猫","pinyin":"Māo","english":"Cat","length":1},{"id":65,"chinese":"没","pinyin":"Méi","english":"Not","length":1},{"id":66,"chinese":"没关系","pinyin":"Méiguānxì","english":"Never mind","length":3},{"id":67,"chinese":"米饭","pinyin":"Mǐfàn","english":"Rice","length":2},{"id":68,"chinese":"名字","pinyin":"Míngzì","english":"Name","length":2},{"id":69,"chinese":"明天","pinyin":"Míngtiān","english":"Tomorrow","length":2},{"id":70,"chinese":"哪（哪儿）","pinyin":"Nǎ (nǎ'er)","english":"What (where)","length":5},{"id":71,"chinese":"那（那儿）","pinyin":"Nà (nà'er)","english":"That (there)","length":5},{"id":72,"chinese":"呢","pinyin":"Ne","english":"It","length":1},{"id":73,"chinese":"能","pinyin":"Néng","english":"Can","length":1},{"id":74,"chinese":"你","pinyin":"Nǐ","english":"You","length":1},{"id":75,"chinese":"年","pinyin":"Nián","english":"Years","length":1},{"id":76,"chinese":"女儿","pinyin":"Nǚ'ér","english":"Daughter","length":2},{"id":77,"chinese":"朋友","pinyin":"Péngyǒu","english":"Friend","length":2},{"id":78,"chinese":"漂亮","pinyin":"Piàoliang","english":"Beautiful","length":2},{"id":79,"chinese":"苹果","pinyin":"Píngguǒ","english":"Apple","length":2},{"id":80,"chinese":"七","pinyin":"Qī","english":"Seven","length":1},{"id":81,"chinese":"前面","pinyin":"Qiánmiàn","english":"Front","length":2},{"id":82,"chinese":"钱","pinyin":"Qián","english":"Money","length":1},{"id":83,"chinese":"请","pinyin":"Qǐng","english":"Please","length":1},{"id":84,"chinese":"去","pinyin":"Qù","english":"Go","length":1},{"id":85,"chinese":"热","pinyin":"Rè","english":"Hot","length":1},{"id":86,"chinese":"人","pinyin":"Rén","english":"People","length":1},{"id":87,"chinese":"认识","pinyin":"Rènshi","english":"Understanding","length":2},{"id":88,"chinese":"日","pinyin":"Rì","english":"Day","length":1},{"id":89,"chinese":"三","pinyin":"Sān","english":"Three","length":1},{"id":90,"chinese":"商店","pinyin":"Shāngdiàn","english":"Shop","length":2},{"id":91,"chinese":"上","pinyin":"Shàng","english":"On","length":1},{"id":92,"chinese":"上午","pinyin":"Shàngwǔ","english":"Morning","length":2},{"id":93,"chinese":"少","pinyin":"Shǎo","english":"Less","length":1},{"id":94,"chinese":"十","pinyin":"Shí","english":"Ten","length":1},{"id":95,"chinese":"什么","pinyin":"Shénme","english":"What","length":2},{"id":96,"chinese":"时候","pinyin":"Shíhou","english":"Time","length":2},{"id":97,"chinese":"是","pinyin":"Shì","english":"Be","length":1},{"id":98,"chinese":"书","pinyin":"Shū","english":"Book","length":1},{"id":99,"chinese":"谁","pinyin":"Shuí","english":"Who","length":1},{"id":100,"chinese":"水","pinyin":"Shuǐ","english":"Water","length":1},{"id":101,"chinese":"水果","pinyin":"Shuǐguǒ","english":"Fruit","length":2},{"id":102,"chinese":"睡觉","pinyin":"Shuìjiào","english":"Sleep","length":2},{"id":103,"chinese":"说话","pinyin":"Shuōhuà","english":"Speak","length":2},{"id":104,"chinese":"四","pinyin":"Sì","english":"Four","length":1},{"id":105,"chinese":"岁","pinyin":"Suì","english":"Years","length":1},{"id":106,"chinese":"他","pinyin":"Tā","english":"He","length":1},{"id":107,"chinese":"太","pinyin":"Tài","english":"Too","length":1},{"id":108,"chinese":"天气","pinyin":"Tiānqì","english":"Weather","length":2},{"id":109,"chinese":"听","pinyin":"Tīng","english":"Listen","length":1},{"id":110,"chinese":"同学","pinyin":"Tóngxué","english":"Classmate","length":2},{"id":111,"chinese":"喂","pinyin":"Wèi","english":"Feed","length":1},{"id":112,"chinese":"我","pinyin":"Wǒ","english":"I","length":1},{"id":113,"chinese":"我们","pinyin":"Wǒmen","english":"We","length":2},{"id":114,"chinese":"五","pinyin":"Wǔ","english":"Five","length":1},{"id":115,"chinese":"喜欢","pinyin":"Xǐhuan","english":"Like","length":2},{"id":116,"chinese":"下","pinyin":"Xià","english":"Next","length":1},{"id":117,"chinese":"下午","pinyin":"Xiàwǔ","english":"Afternoon","length":2},{"id":118,"chinese":"下雨","pinyin":"Xià yǔ","english":"Rain","length":2},{"id":119,"chinese":"先生","pinyin":"Xiānshēng","english":"Mr.","length":2},{"id":120,"chinese":"现在","pinyin":"Xiànzài","english":"Now","length":2},{"id":121,"chinese":"想","pinyin":"Xiǎng","english":"Want","length":1},{"id":122,"chinese":"小","pinyin":"Xiǎo","english":"Small","length":1},{"id":123,"chinese":"小姐","pinyin":"Xiǎojiě","english":"Miss","length":2},{"id":124,"chinese":"些","pinyin":"Xiē","english":"Some","length":1},{"id":125,"chinese":"写","pinyin":"Xiě","english":"Write","length":1},{"id":126,"chinese":"谢谢","pinyin":"Xièxiè","english":"Thank you","length":2},{"id":127,"chinese":"星期","pinyin":"Xīngqí","english":"Week","length":2},{"id":128,"chinese":"学生","pinyin":"Xuéshēng","english":"Student","length":2},{"id":129,"chinese":"学习","pinyin":"Xuéxí","english":"Learning","length":2},{"id":130,"chinese":"学校","pinyin":"Xuéxiào","english":"School","length":2},{"id":131,"chinese":"一","pinyin":"Yī","english":"A","length":1},{"id":132,"chinese":"衣服","pinyin":"Yīfú","english":"Clothes","length":2},{"id":133,"chinese":"医生","pinyin":"Yīshēng","english":"Medical","length":2},{"id":134,"chinese":"医院","pinyin":"Yīyuàn","english":"Hospital","length":2},{"id":135,"chinese":"椅子","pinyin":"Yǐzi","english":"Chair","length":2},{"id":136,"chinese":"有","pinyin":"Yǒu","english":"Have","length":1},{"id":137,"chinese":"月","pinyin":"Yuè","english":"Month","length":1},{"id":138,"chinese":"再见","pinyin":"Zàijiàn","english":"Goodbye","length":2},{"id":139,"chinese":"在","pinyin":"Zài","english":"In","length":1},{"id":140,"chinese":"怎么","pinyin":"Zěnme","english":"How","length":2},{"id":141,"chinese":"怎么样","pinyin":"Zěnme yàng","english":"How to","length":3},{"id":142,"chinese":"这（这儿）","pinyin":"Zhè (zhè'er)","english":"This (here)","length":5},{"id":143,"chinese":"中国","pinyin":"Zhōngguó","english":"China","length":2},{"id":144,"chinese":"中午","pinyin":"Zhōngwǔ","english":"Noon","length":2},{"id":145,"chinese":"住","pinyin":"Zhù","english":"Live","length":1},{"id":146,"chinese":"桌子","pinyin":"Zhuōzi","english":"Tables","length":2},{"id":147,"chinese":"字","pinyin":"Zì","english":"Word","length":1},{"id":148,"chinese":"昨天","pinyin":"Zuótiān","english":"Yesterday","length":2},{"id":149,"chinese":"坐","pinyin":"Zuò","english":"Sit","length":1},{"id":150,"chinese":"做","pinyin":"Zuò","english":"Do","length":1}],
        _wordsLength = _words.length;


    function _onafterrender() {
        _wordchoiceEl = _selector.get('.wordchoice', this.el);
        _wordsEl[0] = _selector.get('.first', _wordchoiceEl);
        _wordsEl[1] = _selector.get('.second', _wordchoiceEl);
        _wordsEl[2] = _selector.get('.third', _wordchoiceEl);
        _wordsEl[3] = _selector.get('.fourth', _wordchoiceEl);
        _helpEl = document.getElementById('helpview');

        _onresize.call(this);
        this.resizeEvent = new _fs.events.Resize();
        this.resizeEvent.attach(_win, _onresize, this);

        this.clickEvent = new _fs.events.Click({
            autoAttach: true,
            el: _wordchoiceEl,
            handler: _onclick,
            scope: this
        });

        _audioEl = document.createElement('audio');
        _audioEl.preload = 'auto';
        _audioEl.autoplay = true;

        _playNext.call(this);
    }

    function _shuffle(items) {

        var cached = items.slice(0), temp, i = cached.length, rand;

        while (--i) {
            rand = Math.floor(i * Math.random());
            temp = cached[rand];
            cached[rand] = cached[i];
            cached[i] = temp;
        }
        return cached;
    }

    function _playNext() {

        var i, n, list = [];

        for (i = 0; i < 4; ++i) {
            n = Math.round(Math.random() * 10000 % _wordsLength);
            list.push(_words[n]);
        }

        _currentAnswer = list[0];
        _loadAudio();
        list = _shuffle(list);

        for (i = 0; i < 4; ++i) {
            _wordsEl[i].innerHTML = list[i].chinese;
            _wordsEl[i].setAttribute('data-id', list[i].id);
            _wordsEl[i].style['font-size'] = (100 / list[i].length) + '%';
        }

        _updateHelp();
    }

    function _updateHelp() {
        if (!_helpEl) {
            return false;
        }

        var help = '<p style="padding: 5px; text-align: center">';

        if (_hasPinyin) {
            help = help + '<i style="padding-right: 5px;">' + _currentAnswer.pinyin + '</i>';
        }
        if (_hasTranslate) {
            help = help + '<b style="padding-left: 5px;">' + _currentAnswer.english + '</b>';
        }

        _helpEl.innerHTML = help + '</p>';
    }

    function _loadAudio() {
        _audioEl.setAttribute('src', 'audio/' + _currentAnswer.chinese + '.mp3');
        _audioEl.load();
    }

    function _onclick(e) {
        if (e.target.getAttribute('data-id') == _currentAnswer.id) {
            _playNext();
        }
    }

    function _onresize() {
        _wordchoiceEl.style.height = (_win.innerHeight - 100) + 'px';
        _wordchoiceEl.style['font-size'] = (_win.innerHeight - 100) / 3 + 'px';
        _win.scrollTo(0, 0);
    }

    function _onaftercompile() {

        this.pinyinBtn.on('check', function() {
            _hasPinyin = true;
            _updateHelp();
        }, this);

        this.pinyinBtn.on('uncheck', function() {
            _hasPinyin = false;
            _updateHelp();
        }, this);

        this.translateBtn.on('check', function() {
            _hasTranslate = true;
            _updateHelp();
        }, this);

        this.translateBtn.on('uncheck', function() {
            _hasTranslate = false;
            _updateHelp();
        }, this);

    }


    return _parent.subclass({

        constructor: function(opts) {
            var items = ['<div class="ui-grid-b"><div class="ui-block-a">', {
                xtype: 'button',
                config: {
                    text: 'Listen',
                    ui: 'f',
                    icon: '&#128227;',
                    size: 'medium',
                    inline: false
                },
                handler: function() {
                    _audioEl.play();
                }
            }, '</div><div class="ui-block-b">', {
                /*xtype: 'button',
                config: {
                    text: 'Next',
                    ui: 'c',
                    icon: '&#59249;',
                    size: 'large',
                    inline: false
                },
                scope: this,
                handler: _playNext*/
                xtype: 'checkbox',
                ref: 'pinyinBtn',
                config: {
                    label: 'pinyin',
                    size: 'small'
                }
            }, '</div><div class="ui-block-c">', {
                xtype: 'checkbox',
                ref: 'translateBtn',
                config: {
                    label: 'traduction',
                    size: 'small'
                }
            }, '</div></div><div class="ui-grid-solo"><div class="ui-block-a" id="helpview"></div></div><div class="ui-grid-a wordchoice"><div class="ui-block-a first">爱</div><div class="ui-block-b second">八</div><div class="ui-block-a third">爸</div><div class="ui-block-b fourth">杯</div></div>'];

            opts.items = items;

            _parent.prototype.constructor.call(this, opts);

            this.on('aftercompile', _onaftercompile, this);
            this.on('afterrender', _onafterrender, this);
        }

    });

}());