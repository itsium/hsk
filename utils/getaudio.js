/*
*/

var fs = require('fs'),
    path = require('path'),
    wordlist = path.resolve(__dirname, 'wordslist.json'),
    exec = require('child_process').exec;

var cmd = 'wget -q -U Mozilla -O audio/$1.mp3 "http://translate.google.com/translate_tts?ie=UTF-8&tl=zh&q=$1"';

var data = fs.readFileSync(wordlist, {
    encoding: 'utf8'
});

data = data.replace('\n', '');

var words = JSON.parse(data);

var i = -1, len = words.length;

while (++i < len) {
    var command = cmd
        .replace('$1', words[i].chinese)
        .replace('$1', words[i].chinese);

    exec(command, function(err, stdout, stderr) {
        if (err || stderr.length) {
            console.error('Error: ', err, stderr);
        }
    });
}